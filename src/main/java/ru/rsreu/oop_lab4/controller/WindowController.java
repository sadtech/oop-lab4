package ru.rsreu.oop_lab4.controller;

import com.sun.webkit.WebPage;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.web.HTMLEditor;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.WindowEvent;
import javafx.util.Pair;
import ru.rsreu.oop_lab4.app.Windows;
import ru.rsreu.oop_lab4.app.read.Reader;
import ru.rsreu.oop_lab4.app.read.ReaderFile;
import ru.rsreu.oop_lab4.app.save.Save;
import ru.rsreu.oop_lab4.app.save.SaveFile;

import java.io.File;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;


public class WindowController implements Initializable {

    @FXML
    private MenuItem menuOpen, menuSave, menuNew, menuClose, menuCopy, menuPaste, menuCut, menuSearch, menuReplace;

    @FXML
    private Menu menuEdit;

    @FXML
    private HTMLEditor htmlEditor;

    @FXML
    private AnchorPane anchorPane;

    private ToolBar toolBar;
    private ToolBar toolBar1;

    private GridPane gridPane;
    private Reader reader;
    private Save save;


    public void initialize(URL location, ResourceBundle resources) {
        menuOpen.setAccelerator(KeyCombination.valueOf("Ctrl+O"));
        menuSave.setAccelerator(KeyCombination.valueOf("Ctrl+S"));
        menuNew.setAccelerator(KeyCombination.valueOf("Ctrl+N"));
        menuClose.setAccelerator(KeyCombination.valueOf("Ctrl+Q"));
        menuSearch.setAccelerator(KeyCombination.valueOf("Ctrl+F"));
        menuReplace.setAccelerator(KeyCombination.valueOf("Ctrl+G"));
        htmlEditor.setVisible(false);
        menuEdit.setDisable(true);
        menuSave.setDisable(true);
    }

    public void editCut(ActionEvent event) {
        Button button = (Button) toolBar.getItems().get(0);
        button.fire();
    }

    public void editCopy(ActionEvent event) {
        Button button = (Button) toolBar.getItems().get(1);
        button.fire();
    }

    public void editPast(ActionEvent event) {
        Button button = (Button) toolBar.getItems().get(2);
        button.fire();
    }


    public void newFile(ActionEvent event2) {
        menuEdit.setDisable(false);
        menuSave.setDisable(false);

        gridPane = (GridPane) htmlEditor.getChildrenUnmodifiable().get(0);
        toolBar = (ToolBar) gridPane.getChildren().get(0);
        toolBar1 = (ToolBar) gridPane.getChildren().get(1);
        toolBar.getItems().remove(7, 14);
        toolBar.getItems().remove(9, 10);
        toolBar1.getItems().remove(0, 1);
        toolBar1.getItems().remove(6, 9);

        Button butSearch = new Button();
        butSearch.getStyleClass().add("butSearch");
        butSearch.getStyleClass().add("button-pane");
        butSearch.setOnAction(e -> editSearch(e));

        Button butReplace = new Button();
        butReplace.getStyleClass().add("button-pane");
        butReplace.getStyleClass().add("butReplace");
        butReplace.setOnAction(event -> editReplace(event));

        toolBar.getItems().add(new Separator());
        toolBar.getItems().add(butSearch);
        toolBar.getItems().add(butReplace);

        htmlEditor.setVisible(true);
    }

    public void openFile(ActionEvent event) {
        if (!htmlEditor.isVisible()) {
            newFile(event);
        }
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Выберете директорию");
        fileChooser.setInitialDirectory(new File("/Users/upagge/IdeaProjects/Rsreu/oop-lab4/src/main/resources/text"));
        File dir = fileChooser.showOpenDialog(null);
        if (dir != null) {
            reader = new ReaderFile(dir);
            reader.read();
            htmlEditor.setHtmlText(reader.getText());
        } else {
            Windows.alert("Файл не найден", "Выберете корректный файл для чтения", Alert.AlertType.WARNING);
        }
    }


    public void saveFile() {
        StringBuilder pathFile = new StringBuilder();

        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Выберете папку для сохранения");
        directoryChooser.setInitialDirectory(new File("/Users/upagge/IdeaProjects/Rsreu/oop-lab4/src/main/resources/text"));
        File dir = directoryChooser.showDialog(null);

        if (dir != null) {
            pathFile.append(dir.getAbsolutePath()).append("/");
            TextInputDialog dialog = new TextInputDialog("textSave.txt");
            dialog.setTitle("Сокранить как...");
            dialog.setHeaderText(null);
            dialog.setContentText("Введите название файла ");
            Optional<String> result = dialog.showAndWait();
            if (result.isPresent()) {
                pathFile.append(result.get());
                save = new SaveFile(htmlEditor.getHtmlText(), pathFile.toString());
                save.save();
                Windows.alert("Успех", "Файл сохранен", Alert.AlertType.INFORMATION);
            } else {
                Windows.alert("Ошибка", "Файл не сохранен", Alert.AlertType.ERROR);
            }

        }
    }


    public void closeApp() {
        if (Windows.alertYesNo("Сохранить", " Сохранить текущий файл?", Alert.AlertType.WARNING)) {
            saveFile();
        }
        System.exit(0);
    }

    public void editSearch(ActionEvent event) {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("");
        dialog.setHeaderText(null);
        dialog.setContentText("Найти");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            searchText(result.get());
            editSearch(event);
        }
    }

    private void searchText(String text) {
        WebView webView = (WebView) gridPane.getChildren().get(2);
        WebEngine engine = webView.getEngine();
        try {
            Field pageField = engine.getClass().getDeclaredField("page");
            pageField.setAccessible(true);
            WebPage page = (com.sun.webkit.WebPage) pageField.get(engine);
            page.find(text, true, true, false);
        } catch (Exception e) { /* log error could not access page */ }
    }


    public void editReplace(ActionEvent event) {
        Dialog<Pair<String, String>> dialog = new Dialog<>();
        dialog.setTitle("Найти и заменить");

        ButtonType loginButtonType = new ButtonType("Заменить", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(10, 10, 10, 10));

        TextField searchWord = new TextField();
        TextField replaceWord = new TextField();

        grid.add(new Label("Найти:"), 0, 0);
        grid.add(searchWord, 1, 0);
        grid.add(new Label("Заменить:"), 0, 1);
        grid.add(replaceWord, 1, 1);

        Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
        loginButton.setDisable(true);

        searchWord.textProperty().addListener((observable, oldValue, newValue) -> {
            loginButton.setDisable(newValue.trim().isEmpty());
        });

        dialog.getDialogPane().setContent(grid);

        Platform.runLater(searchWord::requestFocus);

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == loginButtonType) {
                return new Pair<>(searchWord.getText(), replaceWord.getText());
            }
            return null;
        });

        Optional<Pair<String, String>> result = dialog.showAndWait();

        result.ifPresent(usernamePassword -> {
            cutText(usernamePassword.getKey(), usernamePassword.getValue());
            editReplace(new ActionEvent());
        });
    }

    private void cutText(String oldText, String newText) {
        searchText(oldText);
        editCut(new ActionEvent());
        Clipboard clipboard = Clipboard.getSystemClipboard();
        ClipboardContent content = new ClipboardContent();
        content.putString(newText);
        clipboard.setContent(content);
        editPast(new ActionEvent());
    }

    private javafx.event.EventHandler<WindowEvent> closeEventHandler = event -> closeApp();

    public javafx.event.EventHandler<WindowEvent> getCloseEventHandler() {
        return closeEventHandler;
    }


}
