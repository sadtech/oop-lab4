package ru.rsreu.oop_lab4.app.read;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class ReaderFile implements Reader {

    private File file;
    private String text;

    public ReaderFile(File file) {
        this.file = file;
    }

    public void read() {
        try (FileReader reader = new FileReader(file)) {
            char[] buf = new char[256];
            text = "";
            int c;
            while ((c = reader.read(buf)) > 0) {
                if (c < 256) {
                    buf = Arrays.copyOf(buf, c);
                }
                text += String.copyValueOf(buf);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            text = "";
        } finally {

        }
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getText() {
        return text;
    }
}
