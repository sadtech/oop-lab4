package ru.rsreu.oop_lab4.app.read;

public interface Reader {

    void read();

    String getText();

}
