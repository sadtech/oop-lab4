package ru.rsreu.oop_lab4.app.license;

import org.apache.commons.codec.digest.DigestUtils;
import ru.rsreu.oop_lab4.app.read.Reader;
import ru.rsreu.oop_lab4.app.read.ReaderFile;
import ru.rsreu.oop_lab4.app.save.Save;
import ru.rsreu.oop_lab4.app.save.SaveFile;

import java.io.File;

public class License {

    public boolean checkLicense() {
        Reader reader = new ReaderFile(new File("/Users/upagge/IdeaProjects/Rsreu/oop-lab4/src/main/resources/license/lic.txt"));
        reader.read();
        String cod = "15af3b085dfdb88981eb9d65e17c7dad";
        if (DigestUtils.md5Hex(reader.getText()).equals(cod)) {
            return true;
        } else {
            return false;
        }
    }

    public void writeLicense(String text) {
        Save save = new SaveFile(text, "/Users/upagge/IdeaProjects/Rsreu/oop-lab4/src/main/resources/license/lic.txt");
        save.save();
    }
}
