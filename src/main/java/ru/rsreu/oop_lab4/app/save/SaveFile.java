package ru.rsreu.oop_lab4.app.save;

import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SaveFile implements Save {

    private String text;
    private String path;

    public SaveFile(String text, String path) {
        this.text = text;
        this.path = path;
    }

    @Override
    public void save() {
        try (FileWriter writer = new FileWriter(path, false)) {
            writer.write(getText(text));
            writer.flush();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public String getText(String htmlText) {
        String result = "";
        Pattern pattern = Pattern.compile("<[^>]*>");
        Matcher matcher = pattern.matcher(htmlText);
        final StringBuffer text = new StringBuffer(htmlText.length());
        while (matcher.find()) {
            matcher.appendReplacement(text, " ");
        }
        matcher.appendTail(text);
        result = text.toString().trim();
        return result;
    }
}
