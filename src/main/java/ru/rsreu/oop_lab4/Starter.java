package ru.rsreu.oop_lab4;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.rsreu.oop_lab4.config.SpringBeans;

public class Starter extends Application {

    public static void main(String[] args) {
        Starter load = new Starter();
        load.run(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        ApplicationContext context = new AnnotationConfigApplicationContext(SpringBeans.class);
        stage = context.getBean(Stage.class);
        stage.getScene().setRoot(FXMLLoader.load(getClass().getResource("/fxml/loader.fxml")));
        stage.getScene().setFill(Color.TRANSPARENT);
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.getIcons().add(new Image("/img/favicon.png"));
        stage.show();
    }

    public void run(String[] args) {
        launch(args);
    }
}
