package ru.rsreu.oop_lab4.load_app;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.rsreu.oop_lab4.app.Windows;
import ru.rsreu.oop_lab4.app.license.License;
import ru.rsreu.oop_lab4.config.SpringBeans;
import ru.rsreu.oop_lab4.controller.WindowController;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LoadController implements Initializable {

    @FXML
    private AnchorPane anchorPane;

    @FXML
    private ProgressBar progressLoad;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        new SplashSleep().start();
    }

    class SplashSleep extends Thread {

        private License license = new License();

        private boolean checkLicense() {
            if (license.checkLicense()) {
                return true;
            } else {
                String tempCod = Windows.inputDialog("Лицензионный код");
                if (tempCod != null) {
                    license.writeLicense(tempCod);
                    return checkLicense();
                } else {
                    return false;
                }
            }
        }

        @Override
        public void run() {
            try {
                progressLoad.setProgress(0.0);
                Timeline timeline = new Timeline();
                KeyValue keyValue = new KeyValue(progressLoad.progressProperty(), 1);
                KeyFrame keyFrame = new KeyFrame(new Duration(5000), keyValue);
                timeline.getKeyFrames().add(keyFrame);

                timeline.play();
                sleep(5000);
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        if (checkLicense()) {
                            ApplicationContext context = new AnnotationConfigApplicationContext(SpringBeans.class);
                            Stage stage = context.getBean(Stage.class);
                            try {
                                stage.getScene().setRoot(FXMLLoader.load(getClass().getResource("/fxml/window.fxml")));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            stage.show();
                            stage.getIcons().add(new Image("/img/favicon.png"));
                            anchorPane.getScene().getWindow().hide();
                            stage.setTitle("Word ULTIMATE EDITION");
                            stage.show();
//                            WindowController controller = loader.getController();
//                            stage.setOnCloseRequest(controller.getCloseEventHandler());
                        } else {
                            anchorPane.getScene().getWindow().hide();
                        }
                    }
                });
            } catch (InterruptedException ignored) {

            }
        }
    }
}
