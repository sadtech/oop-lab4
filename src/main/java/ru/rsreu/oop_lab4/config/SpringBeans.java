package ru.rsreu.oop_lab4.config;

import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringBeans {

    @Bean
    public Stage stage() {
        Stage stage = new Stage();
        stage.setScene(scene());
        return stage;
    }

    @Bean
    public Scene scene() {
        Scene scene = new Scene(new AnchorPane());
        return scene;
    }

}
